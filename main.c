/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/

// This is the code to read input from the keypad and actuate the valves when the 'A' button is pressed

#include "main.h"

//GLOBAL VARIABLES


/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void init_GPIOF(void);
void switchOn(uint8_t s);
void switchOff(void);
void vent(void);
void fill(void);
//void init_EXTI(void);

int main(void)
{
  HAL_Init();
  SystemClock_Config();
  init_GPIOF();

  //init_EXTI();
  //switchOff();
  vent();
  //fill();
  while (1)
  {
	  //if(GPIOF->IDR & GPIO_IDR_IDR_2 ){switchOn(0);}
	  //switchOn(fill);fill = !fill;



  }

}

void init_GPIOF(void)
{
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN;
	GPIOF->MODER |= GPIO_MODER_MODE7_0|GPIO_MODER_MODE4_0; // PF4, PF7 OUTPUTS FOR FILLING AND VENTING RESPECTIVELY

	GPIOF->MODER &= ~(GPIO_MODER_MODE2); // PF2 INPUT
	GPIOF->PUPDR |= GPIO_PUPDR_PUPD2_1; // PULL DOWN

}

void switchOn(uint8_t s)
{
	// 0 for venting, 1 for filling
	if(s){GPIOF->ODR = 1<<7;} // PF7 FOR FILLING

	else{GPIOF->ODR = 1<<4;} // PF4 FOR VENTING
}

void switchOff(void)
{
	// switch off valves
	GPIOF->ODR = 0;
}


void vent(void)
{
	while((GPIOF->IDR & GPIO_IDR_IDR_2) == 0){} //wait for button to be pressed
	switchOn(0);
}

void fill(void)
{
	while((GPIOF->IDR & GPIO_IDR_IDR_2) == 0){} //wait for button to be pressed
	switchOn(1);
}


// EXTERNAL INTERRUPT
/*void init_EXTI(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN; // ENABLE CLOCK TO SYSCFG REGISTER
	SYSCFG->EXTICR[0] |= SYSCFG_EXTICR1_EXTI2_PF; // PF2 INTERRUPT PIN on CR1(EXTICR[0])
	EXTI->IMR |= EXTI_IMR_IM2; // UNMASK LINE 2
	EXTI->RTSR |= EXTI_RTSR_TR2; // RISING EDGE TRIGGER ON LINE 2
	NVIC_EnableIRQ(EXTI2_IRQn);
}

void EXTI2IRQHandler(void)
{

    EXTI->PR |= EXTI_PR_PR2;
	fill ^= fill;
	GPIOF->ODR = 1<<4;
	//switchOff();
	//switchOn(fill);

}
*/










//System shit
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 50;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV8;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV4;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}


void Error_Handler(void)
{

  __disable_irq();
  while (1)
  {
  }

}

#ifdef  USE_FULL_ASSERT

void assert_failed(uint8_t *file, uint32_t line)
{

}
#endif /* USE_FULL_ASSERT */

